# VORTEX
OBS: Tudo está funcionando corretamente

### Desafio 1:
  - linguagem:  javascript    
  - versão da linguagem:  mais atual  
  - caminho para os códigos: "vortex/desafiosVortex/desafio1/"  
  - obs: todo o javascript está em "script.js"  
  
  
  
### Desafio 2:
  - linguagem:  java  
  - versão da linguagem:   openjdk 18 2022-03-22    
  - caminho para os códigos: "vortex/desafiosVortex/desafio2/desafio2/src/desafio2/"   
  - obs: "Main.java" é a classe executavel  
  
  
  
### Desafio 3:
  - linguagem:  java  
  - versão da linguagem:   openjdk 18 2022-03-22  
  - caminho para os códigos: "vortex/desafiosVortex/desafio3/poker/src/poker/"  
  - obs: "Game.java" é a classe executavel  
